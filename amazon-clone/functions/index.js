const functions = require('firebase-functions');
const express = require("express");
const cors = require("cors");
const stripe = require("stripe")('sk_test_51I2DDADtPMippBQGc1d2bEMTpS2oEOWdfPQoSBC5yTDhzknktkQQ9FiBF9QZtMDM32TqgTIcS0RYhuQNsvKO7Nxs00kdX3QTyc');

//API 

//App config
const app= express();

//Middlewares
app.use(cors({ origin: true}));
app.use(express.json());
//API routes
app.get("/", (request, response) => response.status(200).send('hello world'));

app.post("/payments/create", async(request, response) =>{
    const total = request.query.total;

    console.log('Payment Request Recieved for this amount:  ', total);

    const paymentIntent = await stripe.paymentIntents.create({
        amount: total,
        currency: "usd",
    })
    //OK-created
    response.status(201).send({
        clientSecret: paymentIntent.client_secret,
    });
});

//listen coomand
exports.api= functions.https.onRequest(app);
//example endpoint
//http://localhost:5001/clone-6e1e7/us-central1/api
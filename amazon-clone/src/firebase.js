import firebase from "firebase";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyDR5tmPWdCqaFkps_gUxX0MoWydHNgPAXs",
    authDomain: "clone-6e1e7.firebaseapp.com",
    projectId: "clone-6e1e7",
    storageBucket: "clone-6e1e7.appspot.com",
    messagingSenderId: "738175858973",
    appId: "1:738175858973:web:f08bbd909c4d08767bc9f4",
    measurementId: "G-GSM0V6ZQYS"
  };

  const firebaseApp = firebase.initializeApp(firebaseConfig);
  const db= firebaseApp.firestore();
  const auth = firebase.auth();


  export { db, auth};
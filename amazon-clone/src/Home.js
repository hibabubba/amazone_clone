import React from "react";
import "./Home.css";
import "./Product"
import Product from "./Product";

function Home(){
    return(
        <div className ="home">
            <div className="home__container">
                <img className="home__banner" src="https://images-eu.ssl-images-amazon.com/images/G/02/digital/video/merch2016/Hero/Covid19/Generic/GWBleedingHero_ENG_COVIDUPDATE__XSite_1500x600_PV_en-GB._CB428684220_.jpg" alt="" />

            <div className="home__row">
                <Product
                id= {1}
                title =" The lean startUp "
                price={79.0}
                image= "https://images-na.ssl-images-amazon.com/images/I/51Zymoq7UnL._AC_SY400_.jpg"
                rating={2}/>

                <Product
                id= {2}
                title =" We hun the flame: A fantasy book set in an Arabian inspired setting "
                price={1.0}
                image= "https://i1.wp.com/www.wehunttheflame.com/wp-content/themes/IceyDesigns-WHTF/images/3Dcover.png?w=640&ssl=1"
                rating={4}/>

            </div>
            <div className="home__row">
            <Product
                id= {3}
                title =" Theft of swords : Fantasy book with thiefs and a dead king "
                price={22.99}
                image= "https://images-na.ssl-images-amazon.com/images/I/91sWCJmJPhL.jpg"
                rating={3}/>
                <Product
                id= {4}
                title =" Blood of my enemies : cup for hot chocolates or coffee "
                price={34.99}
                image= "https://www.chargrilled.com.au/t-shirts/prodimages/imagegen.ashx?vpimageid=24357&tstyle=g&tsize=medium&tcolour=White&tx=0&ty=0&tw=0&tb=False"
                rating={1}/>
                <Product
                id= {5}
                title =" My Secret journal: notebook "
                price={1.00}
                image= "https://ae01.alicdn.com/kf/HTB1JtKXX42rK1RkSnhJq6ykdpXas/HUSH-HUSH-MY-SECRET-DIARY-GOLD-Edition-Journal-Diary-Notebook-With-Lock-BEST-SELLER.jpg_q50.jpg"
                rating={6}/>
                <Product
                id= {12343222}
                title =" A desert scented candle: large "
                price={9.99}
                image= "https://media.osmology.co/wp-content/uploads/2020/06/11165214/Desert-Scented-Candle-by-42Pressed.jpg"
                rating={2}/>
                
            </div>
            <div className="home__row">
            
                <Product
                id= {6}
                title =" Water colour set: for beginners including 50 colours "
                price={42.99}
                image= "https://images-na.ssl-images-amazon.com/images/I/61HqthZ-HYL._AC_SL1001_.jpg"
                rating={5}/>
            </div>
           
            </div> 
        </div>
    )
}


export default Home
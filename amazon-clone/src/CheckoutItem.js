import React from 'react';
import "./CheckoutItem.css";
import { useStateValue } from './StateProvider';

function CheckoutItem({id, price,image, title, rating}) {
    const [{cart},dispatch] = useStateValue();
    
    const removeFromCart=()=>{
        //remove item form cart
        dispatch({
            type: 'REMOVE_FROM_CART',
            id: id, 
        })
    };
        return (
            <div className="checkoutItem">
                <img className="checkoutItem__image" src={image} alt=""/>
                <div className="checkoutItem__info">
                    <p className="checkoutItem__title">
                        <strong> {title} </strong> 
                    </p>
                    <p className="checkoutItem__id">{id} </p>
                    <p className="checkoutItem__price">
                        <small> $ </small>
                        <strong>{price} </strong> 
                    </p>
                    <p className="checkoutItem__rating">
                        {Array(rating)
                        .fill()
                        .map((_,i) => (       
                            <p>⭐</p>
                        ))} 
                    </p>

                    <button onClick={removeFromCart} >Remove from Basket</button>
                </div>
            
            </div>

        )
}

export default CheckoutItem;
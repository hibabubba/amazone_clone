import React, {createContext, useContext, useReducer } from "react";

//Prepares the datalayer
export const StateContext = createContext();

//wrap our app and provide the data layer to every compontent to our app
export const StateProvider= ({reducer, initialState,children}) =>(
    <StateContext.Provider value={useReducer(reducer, initialState)}>
        {children}
    </StateContext.Provider>
);

//pull information from the data layer
export const useStateValue = () => useContext(StateContext);